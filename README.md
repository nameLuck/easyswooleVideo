# easyswooleVideo

#### 介绍
easyswoole与jsmpeg实现简单的直播系统

需要安装ffmpeg做推流，swoole只做转发，jsmpeg为客户端
跑demo的时候建议ffmpeg安装在windown10上面，[官网](https://ffmpeg.zeranoe.com/builds/)有编译好的win版本解压即用

#### 开发环境
PHP 7.3 + swoole 4.4.16

#### 使用说明
1. 启动easyswoole 

    php easyswoole start

    // easyswoole的主服务类型是websocket 
    
    // EasySwooleEvent 里面添加了一个tcp子服务监听9506端口作为接收ffmpeg mpegts格式的视频流
    
2.  jsmpeg目录里面的view-stream.html的var url改成easyswoole的ws地址
3.  ffmpeg执行命令参考：

    播放MP4：ffmpeg -re -i "/path/demoVideo/test.mp4" -f mpegts -c:v mpeg1video -an -s 960x540  -b:v 2048k -r 24 -codec:a mp2 -ar 44100 -ac 1 -b:a 128k http://192.168.16.168:9506 //其中 -re的参数的作用以实际速率播放MP4
    
    录屏：ffmpeg -f gdigrab -i "desktop" -f mpegts -codec:v mpeg1video -s 960*270 -b:v 1300k -r 24 -bf 0 -codec:a mp2 -ar 44100 -ac 1 -b:a 128k http://192.168.16.168:9506
